# 作業ログ

## 2019/08/06
FSMCommandsを使ってみたい。
結論として、undefined reference to はCMakeFileのライブラリ追加不足だった。
つまり、tdaq_add_library のLINK_LIBRARIESの欄にFSMCommands.hに対応するライブラリを入れておく必要があった。
で、そのライブラリ名は、RunControlパッケージのCMakeListを見て、rc_FSMRootであると判断。
ここで、TDAQパッケージのライブラリを入れる際は、tdaq::という名前解決演算子が必要となるので要注意。
これでひとまずL1MuESRODSoftwareパッケージでもコマンド系を使用できることになった！！

## 2019/08/03
### daq::rc::TransitionCmdを使いたい
L1MuESRODSoftwareパッケージでこの関数を使用したいが、普通に実装するとundefined reference to daq::rc::TransitionCmdと言われる。
つまり、CMakeFileのライブラリ追加不足か、ヘッダーファイルのインクルード不足。

しかし、L1MuESRODModule/src/EmulatorMaster.cxxでは特に変更無く使用できたので、ひとまずインクルードしているヘッダーファイルの確認。
面倒なので、EmulatorMaster.cxxでインクルードされているヘッダーファイルすべてをL1MuESRODSoftware/L1MuESRODSoftware/Emulator.hに移植。
するとコンパイルが通らなくなった。

- EmulatorMaster.hをインクルードしていた箇所を削除
- ROSCore.hをインクルードしていた箇所を削除

これでコンパイルが通るように。
改めて、Emulator.cxxでTransitionCmdのインスタンスを作成すると、undefinde...で怒られる。

#### ROSCore/ReadoutModule.hが要か？
ReadoutModule.hをインクルードすると怒られるので、ReadoutModule.hがインクルードしているヘッダーファイルをインクルードしてみる。
同様のエラーで怒られる。

どうやら、ReadoutModule.hを継承したクラスでないと扱えない。。。？諦め

