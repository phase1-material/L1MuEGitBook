# 3. 実装

# C++基本知識
##　各種ファイル
- 実行ファイル
  - プログラムを実行するときに呼び出すファイル
- .oファイル：オブジェクトファイル
  - オブジェクトファイルが複数ある場合は、それらをまとめて（リンク）実行ファイルを作成する
  - g++ hello.cxx -c でオブジェクトファイルのみを作成する
- .soファイル：共有ライブラリ

## 名前マングリング
C++ではC言語と違い同名の関数でもオーバーロードさせて定義することができるので、コンパイラはそれらを区別する必要がある。
そこでコンパイラは適切な関数へのリンクを行うために、シンボルに対して関数名・引数の情報を埋め込む（マングリング）。
こうすることで、関数の多重定義、名前空間の異なる同名のクラス名などに対応できる。
開発段階で、... ```_ZN3TGC14EmulatorMasterC1Ev```みたいに、知っている文字列と変な文字列が入り乱れていたら、このマングリングだなと理解してください。
その上で、デバッグすべき箇所が分かるはず（この例なら、EmulatorMaster.cxx/hに関する何らかのエラーが出ているなと見当がつく。）

## コンパイラー
- g++：GNU(グヌー、オペレーティングシステム)のGCCに含まれるC++コンパイラー。
  - その昔、SRODが100kHzでデータ取得ができなかったときはコンパイルオプションの最適化を試みたこともあった。そこよりも、内部のバッファの大きさ、特に個人的な感想としてmemcpの頻度が
  最もスピードに効いてきていた様に思われるので、適当にバッファを作ったりしていると痛い目をみる。今後も要注意。

## virtual関数
関数の宣言のときに、virtual をつけると仮想関数となる。仮想関数は派生クラスで再定義（オーバーライド）できる。
基底クラスであるComponentBaseのbaseBoot()は仮想関数であり、自身の実装もComponentBase.cxxで行われている。
通常のSRODプロセスはこのComponentBase::baseBoot()を呼び出しているが、エミュレーターを走らせる際に気をつけなければならない。
Emulatorたちは、SRODマシンとは別のマシンで動くため、SRODプロセス用の起動・セットアップを行ってくれる関数を呼び出すとコケる。
それを伏せぐためにも、virtual bool baseBoot();として実装されている。
エミュレーターはEmulator::baseBoot()を呼び出すようにしている。

## 純粋仮想関数
virtual func() = 0; と実装されている関数で、これは派生クラスで必ずオーバーライドしなければならない関数を表す。

## 言語リンケージ
CとC++では関数の呼び方（C++の方がオーバーロード等関数の呼び方が多様であり）が異なるため、関数をどの言語の呼び出し方で実行するか、その方法を言語リンケージという。
C言語で書かれた関数から、C++で記述された関数を呼び出すにはこの言語リンケージをCリンケージと明記して実装しておく必要がある。
```
extern"C"{int func();} // Cリンケージで定義
```

## using namespace stdを使うな！という話
便利だが標準ライブラリは膨大で、自分が定義した関数名が既に定義されているとコンパイルエラーを引き起こす。
絶対使われてない名前だろう、というときはよいが、基本的に名前解決演算子を使ってちゃんとstd::XXXX()と書くのが無難。
TGCソフトウェアはnamespace TGCの下にあるので基本的には名前空間に関して特に気を使う必要はないように思われる。

## キャスト
- static_cast
  - ある型からある型への暗黙のキャストを明示する場合に使用する(intからdoubleなど)。
- dynamic_cast
  - 親クラスへのポインタを派生クラスへのポインタにキャストするときに使用する（=ダウンキャストに使用する）。
- reinterpret_cast
  - ポインタ型を他のポインタ型へ強制的に変換する。
- const_cast
  - ポインタ、参照にconstを付けたり外したりすることができる。C++ではconst宣言されている変数のconstをこのキャストで外せる。

## 関数のdelete/default宣言
クラスを定義すると、デフォルトコンストラクタ、コピーコンストラクタ、ムーブコンストラクタ、コピー代入演算子、ムーブ代入演算子、デストラクタが暗黙的（勝手に）に定義される。
C++ではこれら暗黙定義される関数を制御するためにdefault, delete構文が用意されている。
- delete : 暗黙定義される関数を明示的に禁止するための機能。
  - 例えばデフォルトコンストラクタをdeleteすれば、そのインスタンスを作成することができずコンパイルエラーとすることができる。
  - ちなみにC++11以前は、デフォルトコンストラクタをprivate関数として定義しておけば、これまたインスタンス作成（デフォルトコンストラクタ呼び出し）を禁止できる。

## 静的メンバ関数
実体がなくても呼び出せます。デフォルトコンストラクタをdeleteしておけば、「実体は作るな（デフォルトコンストラクタを呼ぶな）、普通にこの関数を呼べ」みたいなメッセージを
持たせることができます。

## enum class
enum classで定義した列挙型は「列挙型への暗黙の型変換を行わない」、「列挙型のスコープを持つ」という新機能が実装されている。
daq::rc::FSM_COMMANDはenum class型で定義されており、
```

```
数値型からenum型への変換：明示的な型変換（static_cast）が必要。

## シングルトン
アプリケーションでインスタンス数をひとつに制限する実装手法。
たとえば、とあるPCIeカードを扱うクラスのインスタンスはアプリケーション内で一つにしておきたいだとか
（=勝手にコンストラクタを呼べないようにする、実体を共有できるようにする、等の趣向がいる）。
これはstatic修飾子を使うことで実現できる。
### まずstaticメンバ関数のおさらい
```
class A {
  public:
    static void foo(); // 静的メンバ関数
}
```
通常のメンバ関数は各クラスAオブジェクトに固有なものになるが、staticメンバ関数はクラスに固有のものとなる。
メンバ変数にアクセスできなくなるが、インスタンスなしで呼ぶことができるようになる。
```
int main(){
  A::foo(); // インスタンスなしで呼べる！
}
```

### では実際にシングルトンの実装
通常はあるクラスのオブジェクトは作り放題である。
```
class NotSingleton {
  public : 
    SingleTon();
    ~SingleTon();
  private:
  ///
}
```
このNotSingletonクラスのオブジェクトは、
```
NotSingleton obj1;
NotSingleton obj2;
```
の様に通常のように作成することができる。さらに通常obj1とobj2は別の実体である。
シングルトンは、このオブジェクト（インスタンス）を1個しか作れないようにする、もしくは共通のオブジェクトを参照できるようにする、という実装方法である。
次のようにする。
```
class SingleTon {
  private : 
    SingleTon();
    ~SingleTon();
  public : 
    static SingleTon& getInstance() { static SingleTon obj; return obj;};
}
```
まず、デフォルトコンストラクタ、コピーコンストラクタ等をprivateにすることで、インスタンスを作れないようにする。
そのうえで、インスタンス作成の関数をpublicメンバ関数として持っておき、内部でSingleTonクラスのstaticオブジェクトへの参照を返す関数を作成する。
なにで、
```
SingleTon obj; // コンパイルエラー
SingleTon& s  = SingleTon::getInstance(); // OK
SingleTon& s1 = SingleTon::getInstance(); // OK、かつsと同じ実体を参照している（static SingleTonインスタンスを返すので）。
```

## explicit 指定子
値渡しする引数ではコピーコンストラクタ以外のコンストラクタも呼ばれる。
値渡しでコンストラクタを呼ばれないようにするには、コンストラクタの宣言にexplicitをつける。
望ましくない暗黙的型変換を制御することができる。

# SROD Standalone calibration system
SRODシステム単体だけで、デバッグを行うためのモード。
実際にSRODソフトウェアを走らせるPCと、SL/TTCエミュレーターを走らせるPCの最低2台を使用する。
エミュレーターアプリケーションはTDAQの環境に載っているので、IGUI上からコントロールすることが可能となる。
SLファームウェアを専用に作ってもらわなくても、任意のデータパターン等の試験を行うことができる。
### 
- L1MuESRODModule/
  - EmulatorModule.h
    - エミュレーター用のRunControl Driver (RCD)。TDAQ環境で動作させるために（TDAQコマンドをハンドルするために）、TDAQソフトウェアのROSCoreをpublic継承している。
    - externで宣言している
  - EmulatorMaster.h
    - EmulatorModuleで実際にどういう処理を行うか、具体的な実装を行っている。

### TDAQアプリケーション
TDAQ環境で実行ファイルを動かすには、具体的な処理をさせたいアプリケーションを作成し、またそれらとコマンドをやり取りするRunControl Driverを作成する。
#### RunControl Driver (RCD)
まず、ROS::ReadoutModuleクラスを継承したクラスを作成する必要がある。

## TDAQ State
ATLASすべての検出器・システムはTDAQステートで同期される必要がある。
各RootControllerがその子Controllerたちに現在のステートを揃えて、次のステートへと進む。このときにとあるコンフィギュレーションが時間がかかりすぎると、
他のグループに迷惑がかかることにも繋がるので、パパっと設定できるに越したことはない。
ステートとコマンドがあって、コマンドはステートを遷移させるためのものである。

TGC::ComponentBaseクラスは、新しいTDAQコマンドが降ってきたときに（ステートが遷移するときに）
実行される関数を選択するための関数(ComponentBase::doTDAQCommandFunc())を用意している。
また、それとは別に各ステートでSRODシステムがどのような振る舞いをするのかを示した関数（ComponentBase::doTDAQStateFunc()）も用意されている。

| TDAQ State | TDAQ Command | RCD (XXXModule.cxx) | \*Module function | SROD process function |
|:---:|:---:|:---:|:---:|:---:|
| NONE        |               |                   | |
| &darr;      | INITIALIZE    | setup();          | |
| INITIAL     |               |                   | |
| &darr;      | CONFIGURE     | configure();      | daqConfigure();|
| CONFIGURED  |               |                   | |
| &darr;      | CONNECT       | connect();        | daqConnect();|
| CONNECTED   |               |                   | |
| &darr;      | START         | prepareForRun();  | daqPrepareForRun();|
| RUNNING     |               |                   | daqRun();|
|             | STOPROIB      | | |
| ROIBSTOPPED |               | | |
|             | STOPDC        | | |
| DCSTOPPED   |               | | |
|             | STOPHLT       | | |
| HLTSTOPPED  |               | | |
|             | STOPRECORDING | | |
| SFOSTOPPED  |               | | |
|             | STOPCAHTERING | | |
| GTHSTOPPED  |               | | |
|             | STOPARCHIVING | | |
| CONNECTED   |               | | |
|             | DISCONNECT    | | |
| CONFIGURED  |               | | |
|             | UNCINFIGURE   | unconfigure(); ~Destructor();| |
| INITIAL     |               | | |
|             | SHUTDOWN      | | |
| NONE        |               | | |

## TGCソフトウェアにおけるTDAQコマンド
ROSCoreをpublic継承した各モジュール系クラス（L1MuESLModule, L1MuESRODModule）はRCDに対応していて、直接TDAQコマンドを関数の引数として受け取っている。
それらをTGCシステム内部のプロセスたちにどうやって分配しているか。

まずTDAQコマンド・ステートはそれらを扱うようのクラスがtdaq側で用意されているのでそれを使用する。
各RCDはdaq::rc::TransitionCmdを引数に受け取り、そこから分配されたコマンド・ステートを読み取ることができる。
https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/doxygen/nightly/html/d4/d13/classdaq_1_1rc_1_1TransitionCmd.html

### daq::rc::FSMCommands
https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/doxygen/nightly/html/dc/d9c/classdaq_1_1rc_1_1FSMCommands.html
TDAQパッケージが提供しているFSMコマンドハンドル用のクラス。
- stringToCommand : TDAQコマンドをintへ変換することができる。SRODでは共有メモリにコマンド情報を書き込む必要があるので、stringではなくintへ変換し書き込んでいる。
- commandToString : Convert the daq::rc::FSM_COMMAND item to its string name. 

#### FSMCommands
FSMCommands.hで列挙型で定義されている。
そうそうこのenumの並びは変わらないと思うが（これが変わるとTDAQシステムは全く動かなくなると思うので）、何らかのアナウンスがあった場合は要確認ということで。

| コマンド名       | enum int |
|:---:|:---:|
| INIT_FSM         | 0 |
| INITIALIZE       | 1 |
| CONFIGURE        | 2 |
| CONFIG           | 3 |
| CONNECT          | 4 |
| START            | 5 |
| STOP             | 6 |
| STOPROIB         | 7 |
| STOPDC           | 8 |
| STOPHLT          | 9 |
| STOPRECORDING    | 10 |
| STOPGATHERING    | 11 |
| STOPARCHIVING    | 12 |
| DISCONNECT       | 13 |
| UNCONFIG         | 14 |
| UNCONFIGURE      | 15 |
| SHUTDOWN         | 16 |
| STOP_FSM         | 17 |
| USERBROADCAST    | 18 |
| RELOAD_DB        | 19 |
| RESYNCH          | 20 |
| NEXT_TRANSITION  | 21 |
| SUB_TRANSITION   | 22 |

### daq::rc::FSMStates
https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/doxygen/nightly/html/d2/d7b/classdaq_1_1rc_1_1FSMStates.html
- stringToState   : TDAQステートをintへ変換することができる。SRODでは共有メモリにステート情報を書き込む必要があるので、stringではなくintへ変換し書き込んでいる。

### SROD
SRODではRCDが受け取ったコマンドを共有メモリに書き込み、各アプリケーションはそのメモリをポーリングし、ステートの遷移に備えている。
実装的にはコマンドを分配するのではなく、各プロセスが自ら貰いに行っている形になっている。

### RCMemory
TDAQコマンドを配分するための共有メモリ。SRODの走るマシンの/dev/shm以下に作成されるファイルであり、各プロセスは自分の共有メモリ領域を/dev/shm以下に確保する。
RCMemory::createRCMemory()が呼び出されると、第二引数で指定されたバイト数の共有メモリをまず作成し、次にメモリ領域の先頭から順に以下の様に分割したポインタを作成する。


| 確保される領域 | ポインタ名 | 意味 | 
|:---:|:---:|:---:|
| sizeof(unsigned int)       | p_state              | ステート                              |
| sizeof(unsigned int)       | p_command            | コマンド                              |
| sizeof(unsigned int)       | p_eb_fifo_status_ttc | EventBuilderのTTCCollector用のFIFO情報|
| sizeof(unsigned int) 12SL分| p_eb_fifo_status_sl  | EventBuilderのSLCollector用のFIFO情報 |
| sizeof(unsigned int)       | p_rod_buffer_status  | RODバッファの情報                     |
| sizeof(unsigned int)       | p_error_code         | エラーコード                          |
| sizeof(unsigned int)       | p_total_events       | 処理したイベント数                    |
| sizeof(unsigned int)       | p_process_id         | 自身のプロセスID                      |

## OKSの構造

# 各パッケージの説明
* ~Moudle：各セグメントのRCDに対応している。

# CMakeListの書き方 (tdaq07)
なにか新しいライブラリを作成するときはtdaq_add_libraryを使用する。
クラス名と作成するライブラリ名を統一しておかなければ、TDAQを走らせるときに undefined symbolと怒られるので注意。


## ソケット通信
プロセス間通信の手段として、SRODではソケット通信を用いている。
基本的に通常はSRODがクライアント、SL, TTC F/Oがサーバー側としてポートを空けてTCP/IP通信を行う。

| サーバー | クライアント |
|:---:|:---:|
| ソケットを開く                       | ソケットを開く |
| オプションを設定する                 |                |
| listen                               | 接続要求をする |
| クライアントからの接続要求を承認する |                |
| send/recv | send/recv |

各アクションに対応するシステムコールを正確に呼べるかが鍵である。

### netstat 
ただしくソケット接続が確立されているかを確認することができる。






